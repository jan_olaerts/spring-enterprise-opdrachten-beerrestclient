package be.janolaerts.enterprisespringopdrachten;

import be.janolaerts.enterprisespringopdrachten.domain.Beer;
import be.janolaerts.enterprisespringopdrachten.domain.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.domain.BeerOrderItem;
import be.janolaerts.enterprisespringopdrachten.restclient.BeerClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
public class BeerRestClientApplication {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.requestFactory(
                HttpComponentsClientHttpRequestFactory::new)
                .basicAuthentication("Jan", "test")
                .build();
    }

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
            SpringApplication.run(BeerRestClientApplication.class, args);

        BeerClient beerClient = ctx.getBean("beerRestClient", BeerClient.class);
        List<Beer> beers;

        // Get beer by id
        Beer beer = beerClient.getBeerById(72);
//        System.out.println(beer);

        // Get beers by alcohol
        beers = beerClient.getBeersByAlcohol(9f);
//        beers.forEach(System.out::println);

        // Get beers by name
        beers = beerClient.getBeersByName("kriek");
//        beers.forEach(System.out::println);

        BeerOrder beerOrder;
        // Make a beer order

        beerOrder = new BeerOrder("Rest Beer Order Jan", List.of(
                new BeerOrderItem(beerClient.getBeerById(72), 15),
                new BeerOrderItem(beerClient.getBeerById(51), 5),
                new BeerOrderItem(beerClient.getBeerById(111), 1),
                new BeerOrderItem(beerClient.getBeerById(891), 2)
        ));

        beerClient.createBeerOrder(beerOrder);

        // Get a beer order
//        beerOrder = beerClient.getBeerOrderById(103);
//        beerOrder.getItems().forEach(boi -> {
//            System.out.println(boi.getBeer().getId() + " : " + boi.getAmount());
//        });
    }
}