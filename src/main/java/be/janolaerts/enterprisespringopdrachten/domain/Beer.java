package be.janolaerts.enterprisespringopdrachten.domain;

import java.io.Serializable;
import java.util.Arrays;

public class Beer implements Serializable {

    private int id;
    private String name;
    private Brewer brewer;
    private Category category;
    private double price;
    private int stock;
    private float alcohol;
    private int version;
    private byte[] image;

    public Beer() {
    }

    public Beer(String name, double price, int stock, float alcohol) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.alcohol = alcohol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brewer getBrewer() {
        return brewer;
    }

    public void setBrewer(Brewer brewer) {
        this.brewer = brewer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(float alcohol) {
        this.alcohol = alcohol;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brewer=" + brewer +
                ", category=" + category +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                ", version=" + version +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}