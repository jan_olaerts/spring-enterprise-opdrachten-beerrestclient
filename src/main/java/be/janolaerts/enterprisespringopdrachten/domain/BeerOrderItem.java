package be.janolaerts.enterprisespringopdrachten.domain;

import javax.xml.bind.annotation.XmlTransient;

public class BeerOrderItem {

    private int id;
    private Beer beer;
    private int amount;

    public BeerOrderItem() {
    }

    public BeerOrderItem(Beer beer, int amount) {
        this.beer = beer;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    @XmlTransient
    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int number) {
        this.amount = number;
    }

    @Override
    public String toString() {
        return "BeerOrderItem{" +
                "id=" + id +
                ", beer=" + beer +
                ", number=" + amount +
                '}';
    }
}