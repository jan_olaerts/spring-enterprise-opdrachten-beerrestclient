package be.janolaerts.enterprisespringopdrachten.restclient;

import be.janolaerts.enterprisespringopdrachten.domain.Beer;
import be.janolaerts.enterprisespringopdrachten.domain.BeerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class BeerRestClient implements BeerClient{

    private String baseURL;
    private RestTemplate template;

    @Value("http://localhost:8080/rest")
    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    @Autowired
    public void setTemplate(RestTemplate template) {
        this.template = template;
    }

    @Override
    public Beer getBeerById(int id) {

        ResponseEntity<Beer> response = template
                .getForEntity(baseURL + "/beers/{id}", Beer.class, id);

        if(response.getStatusCode() == HttpStatus.OK)
            return response.getBody();

        else
            return null;
    }

    @Override
    public List<Beer> getBeersByAlcohol(float alcohol) {

        ResponseEntity<Beer[]> response = template
                .getForEntity(baseURL + "/beers?alcohol= " + alcohol, Beer[].class);

        if(response.getStatusCode() == HttpStatus.OK)
            return Arrays.asList(Objects.requireNonNull(response.getBody()));

        else
            return null;
    }

    @Override
    public List<Beer> getBeersByName(String name) {

        ResponseEntity<Beer[]> response = template
                .getForEntity(baseURL + "/beers?name= " + name, Beer[].class);

        if(response.getStatusCode() == HttpStatus.OK)
            return Arrays.asList(Objects.requireNonNull(response.getBody()));

        else
            return null;
    }

    @Override
    public BeerOrder getBeerOrderById(int id) {

        ResponseEntity<BeerOrder> response = template
                .getForEntity(baseURL + "/orders/{id}", BeerOrder.class, id);

        if(response.getStatusCode() == HttpStatus.OK)
            return response.getBody();

        else
            return null;
    }

    @Override
    public URI createBeerOrder(BeerOrder beerOrder) {
        return template.postForLocation(baseURL + "/orders", beerOrder);
    }
}