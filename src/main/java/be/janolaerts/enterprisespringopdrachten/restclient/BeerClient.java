package be.janolaerts.enterprisespringopdrachten.restclient;

import be.janolaerts.enterprisespringopdrachten.domain.Beer;
import be.janolaerts.enterprisespringopdrachten.domain.BeerOrder;

import java.net.URI;
import java.util.List;

public interface BeerClient {

    Beer getBeerById(int id);
    List<Beer> getBeersByAlcohol(float alcohol);
    List<Beer> getBeersByName(String name);
    BeerOrder getBeerOrderById(int id);
    URI createBeerOrder(BeerOrder beerOrder);
}